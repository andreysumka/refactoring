package Refactoring;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Рефакторинг и оптимизация кода
 *
 * @author Sumka Andrey 18it18
 */

public class Refactor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[5];
        for (int index = 0; index < 5; index++) {
            array[index] = -25 + (int) (Math.random() * 51);
        }
        System.out.println(Arrays.toString(array));
        System.out.println("Введите число которое нужно найти: ");
        int numbInArray = scanner.nextInt();
        SearchElement(array, numbInArray);
    }


    /**
     * Метод по поиску элемента массива
     *
     * @param array массив
     * @param numbInArray элемент массива
     */
    public static void SearchElement(int[] array, int numbInArray) {
        int number = -1;
        for (int index = 0; index < 5; index++) {
            if (array[index] == numbInArray) {
                number = index;
            }
        }
        if (number == -1) {
            System.out.println("Нет такого значения");
        } else {
            System.out.println("Номер элемента в последовательности" + number);
        }
    }
}

